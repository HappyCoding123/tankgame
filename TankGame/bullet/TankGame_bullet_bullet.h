#ifndef BULLET_H
#define BULLET_H
#include<iostream>
#include<math.h>
#include<QPainter>

class bullet1{
private:
    int angel;              //子弹运动角度
    int bulletpower;        //子弹威力
    int bulletR;            //子弹半径
    int bulletV;            //子弹速度
    int bulletexist;        //子弹状态
public:
    int x;                  //子弹x坐标
    int y;                  //子弹y坐标
    bullet1();
    void bullet1move();
    void bullet1create(QPainter *q);
};

class bullet2{
private:
    int startx;                 //激光起始x坐标
    int starty;                 //激光起始y坐标
    int endx;                   //激光终止x坐标
    int endy;                   //激光终止y坐标
    int angel;                  //激光角度
    int bullet2power;           //激光威力
    int bulletwidth;            //激光直线宽度
    int bullet2exist;           //激光状态
public:
    bullet2();
    void getendxy();
    void bullet2create(QPainter *q);
};


#endif // BULLET_H
