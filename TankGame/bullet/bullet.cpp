#include "bullet.h"
#define c 20

int  static map[50][50] ;
bullet1::bullet1(){
    x = 0;
    y = 0;
    angel = 0;
    bulletpower = 5;
    bulletR = 5;
    bulletV = 10;
    bulletexist = 1;
}
void bullet1::bullet1move(){
    int i;
    for (i = 0;map[bullet1::x / c][bullet1::y / c] != 3; i++) {
    x = x + int (bulletV * cos(angel));
    y = y + int (bulletV * sin(angel));
    }
}

bullet2::bullet2(){
    startx = 0;
    starty = 0;
    endx = 0;
    endy = 0;
    angel = 0;
    bullet2power = 10;
    bulletwidth = 10;
    bullet2exist = 1;
}

void bullet2::getendxy(){
       int i;
       int d;
       d =int (c / cos(angel));
       bullet2::endx = bullet2::startx;
       bullet2::endy = bullet2::starty;
       for(i = 0;map[bullet2::endx / c][bullet2::endy / c] != 3;i = i + d){

           bullet2::endx = bullet2::endx + int (d * cos(angel));
           bullet2::endy = bullet2::endy + int (d * sin(angel));
       }
}

void bullet1::bullet1create(QPainter *q){

    QPixmap img;
    img.load("../image/2772119_103315047_2.jpg");           //贴子弹图
       q->drawPixmap(this->x,this->y,5,5,img);
}

void bullet2::bullet2create(QPainter *q){
    QPen pen;
    pen.setWidth(10);
    pen.setColor(QColor(140,145,150));
    q->setPen(pen);
    q->drawLine(this->startx,this->starty,this->endx,this->endy);   //画一条激光
}
