#ifndef CHECK_MAP_H
#define CHECK_MAP_H
#include "check_map.h"
#include <iostream>
#include <math.h>
int a,b,c,e,f;      //依次为坦克的宽长，地形的边长，敌方坦克的宽长
int direction;
int army_numb;
int Map[100][100];             //地图元素的隐含地图，第一个【】就代表横向x坐标，第二个代表纵向y坐标，原点为左下角
// 0 ：空白格 （墙壁类）{3：墙壁 4：铁墙} （debuff类）{5：沼泽 6：河流} （功能类）{7：桥 8：穿越洞}（道具类）{10，11，12}

class check_map
{
    int y;
public:
    check_map();
};

int check_1(int x,int y,int direction)   //当θ为0 或者 PI 时使用
{
    int y1,x1,x2;
    int middle=y+(b/2)*direction;
    x1=(x-a/2)/c;
    x2=(x+a/2)/c;
    if((middle/c)*c==middle)
    {
        if(direction==1)
        {
            y1=middle/c+1;
            int i;
            for(i=x1;i<=x2;i++)
            {
                if(Map[i][y1]==3||Map[i][y1]==4)           //不能通行，如果需要添加别的功能块，如沼泽，可以加入elseif和其他的return来编写
                {
                    return 0;
                    break;
                }
            }
        }
        else if (direction==-1)
        {
            y1=middle/c;
            int i;
            for(i=x1;i<=x2;i++)
            {
                if(Map[i][y1]==3||Map[i][y1]==4)           //不能通行，如果需要添加别的功能块，如沼泽，可以加入elseif和其他的return来编写
                {
                    return 0;
                    break;
                }
            }
        }
    }
    return 1;
}

int check_2(int x,int y,int direction)   //当θ为 PI/2 或者 3PI/2 时使用
{
    int x1,y1,y2;
    int middle;
    middle=x+b/2*direction;
    y1=(y+a/2)/c+1;
    y2=(y-a/2)/c+1;
    if((middle/c)*c==middle)
    {
        if(direction==1)
        {
            x1=middle/c;
            int i;
            for (i=y2;i<=y1;i++) {
                if(Map[x1][i]==3||Map[x1][i]==4)           //不能通行，如果需要添加别的功能块，如沼泽，可以加入elseif和其他的return来编写
                {
                    return 0;
                    break;
                }
            }
        }
         else
        {
            x1=middle/c-1;
            int i;
            for (i=y2;i<=y1;i++)
            {
                if(Map[x1][i]==3||Map[x1][i]==4)           //不能通行，如果需要添加别的功能块，如沼泽，可以加入elseif和其他的return来编写
                {
                    return 0;
                    break;
                }
            }
        }
    }
    return 1;
}

int check_3(int x,int y,double z)        //和地形相撞
{
    int x1,x2,y1,y2;
    double k[4];
    k[0]=y-atan(z)*x+a/(2*abs(sin(z)));
    k[1]=y-atan(z)*x-a/(2*abs(sin(z)));
    k[2]=tan(z)*x+y+b/(2*abs(cos(z)));
    k[3]=tan(z)*x+y-b/(2*abs(cos(z)));
    int i,j;
    double middle=sqrt(a*a+b*b);
    x1=((x-middle)/c-1)*c;                 //检验地图的四个边界
    x2=((x+middle)/c+1)*c;
    y1=((y-middle)/c)*c;
    y2=((y+middle)/c+2)*c;
    int Tank0_x[4];
    int Tank0_y[4];
    double middle0=1/(atan(z)+tan(z));
    Tank0_x[0]=(k[2]-k[0])*middle0;                       //对应坦克四个顶点的坐标
    Tank0_x[1]=(k[2]-k[1])*middle0;
    Tank0_x[2]=(k[3]-k[1])*middle0;
    Tank0_x[3]=(k[3]-k[0])*middle0;
    Tank0_y[0]=atan(z)*Tank0_x[0]+k[0];
    Tank0_y[1]=atan(z)*Tank0_x[1]+k[1];
    Tank0_y[2]=atan(z)*Tank0_x[2]+k[1];
    Tank0_y[3]=atan(z)*Tank0_x[3]+k[0];
    for(i=x1;i<=x2;i++)
    {
        for (j=y1;j<=y2;j++)
        {
            if(Map[i][j]==3||Map[i][j]==4)
            {
                if(j<=atan(z)*i+k[0]&&j>=atan(z)*i+k[1]&&j>=-tan(z)*i+k[3]&&j<=-tan(z)*i+k[2])                //某个障碍点的一个角到了坦克内部
                {
                    return 0;
                    break;
                }
                else if(j<=atan(z)*(i+c)+k[0]&&j>=atan(z)*(i+c)+k[1]&&j>=-tan(z)*(i+c)+k[3]&&j<=-tan(z)*(i+c)+k[2])
                {
                    return 0;
                    break;
                }
                else if(j-c<=atan(z)*i+k[0]&&j-c>=atan(z)*i+k[1]&&j-c>=-tan(z)*i+k[3]&&j-c<=-tan(z)*i+k[2])
                {
                    return 0;
                    break;
                }
                else if(j-c<=atan(z)*(i+c)+k[0]&&j-c>=atan(z)*(i+c)+k[1]&&j-c>=-tan(z)*(i+c)+k[3]&&j-c<=-tan(z)*(i+c)+k[2])
                {
                    return 0;
                    break;
                }
                else                                  //己方坦克的一个角进入了障碍物
                {
                    int t;
                    for(t=0;t<=3;t++)
                    {
                        if(Tank0_x[t]>=i&&Tank0_x[t]<=i+c&&Tank0_y[t]>=j-c&&Tank0_y[t]<=j)
                        {
                            return 0;
                            break;
                        }
                    }
                }
               /* else if(Tank0_x[0]>=i&&Tank0_x[0]<=i+c&&Tank0_y[0]>=j-c&&Tank0_y[0]<=j)              //坦克的一个角跑到了障碍物里
                {
                    return 0;
                    break;
                }
                else if(Tank0_x[1]>=i&&Tank0_x[1]<=i+c&&Tank0_y[1]>=j-c&&Tank0_y[1]<=j)
                {
                    return 0;
                    break;
                }
                else if(Tank0_x[2]>=i&&Tank0_x[2]<=i+c&&Tank0_y[2]>=j-c&&Tank0_y[2]<=j)
                {
                    return 0;
                    break;
                }
                else if(Tank0_x[3]>=i&&Tank0_x[3]<=i+c&&Tank0_y[3]>=j-c&&Tank0_y[3]<=j)
                {
                    return 0;
                    break;
                }*/
            }

        }
    }
    return 1;
}

int check_4(int x1,int y1,int x2,int y2)           //检验己方坦克和敌方坦克是否在判定范围内
{
    if(sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2))<=(sqrt(e*e+f*f)/2+sqrt(a*a+b*b)/2))
        return 1;                                  //需要进行进一步检测
    else
        return 0;                                 //不需要进行进一步检测
}

int check_5(int x01,int y01,double z1,int x02,int y02,double z2)          //和坦克相撞
{
    int Tank0_x[4];                                     //己方坦克四个点的横坐标
    int Tank0_y[4];                                     //己方坦克四个点的纵坐标
    int Tank1_x[4];                                     //敌方坦克四个点的横坐标
    int Tank1_y[4];                                     //敌方坦克四个点的纵坐标
    double middle0=1/(atan(z1)+tan(z1));
    double middle1=1/(atan(z2)+tan(z2));
    double k0[4];                                      //己方坦克的边界函数截距
    double k1[4];                                      //敌方塔克的边界函数截距
    k0[0]=y01-atan(z1)*x01+a/(2*abs(sin(z1)));
    k0[1]=y01-atan(z1)*x01-a/(2*abs(sin(z1)));
    k0[2]=tan(z1)*x01+y01+b/(2*abs(cos(z1)));
    k0[3]=tan(z1)*x01+y01-b/(2*abs(cos(z1)));
    k1[0]=y02-atan(z2)*x02+e/(2*abs(sin(z2)));
    k1[1]=y02-atan(z2)*x02-e/(2*abs(sin(z2)));
    k1[2]=tan(z2)*x02+y02+f/(2*abs(cos(z2)));
    k1[3]=tan(z2)*x02+y02-f/(2*abs(cos(z2)));
    Tank0_x[0]=(k0[2]-k0[0])*middle0;
    Tank0_x[1]=(k0[2]-k0[1])*middle0;
    Tank0_x[2]=(k0[3]-k0[1])*middle0;
    Tank0_x[3]=(k0[3]-k0[0])*middle0;
    Tank0_y[0]=atan(z1)*Tank0_x[0]+k0[0];
    Tank0_y[1]=atan(z1)*Tank0_x[1]+k0[1];
    Tank0_y[2]=atan(z1)*Tank0_x[2]+k0[1];
    Tank0_y[3]=atan(z1)*Tank0_x[3]+k0[0];

    Tank1_x[0]=(k1[2]-k1[0])*middle1;
    Tank1_x[1]=(k1[2]-k1[1])*middle1;
    Tank1_x[2]=(k1[3]-k1[1])*middle1;
    Tank1_x[3]=(k1[3]-k1[0])*middle1;
    Tank1_y[0]=atan(z2)*Tank1_x[0]+k1[0];
    Tank1_y[1]=atan(z2)*Tank1_x[1]+k1[1];
    Tank1_y[2]=atan(z2)*Tank1_x[2]+k1[1];
    Tank1_y[3]=atan(z2)*Tank1_x[3]+k1[0];
    int i;
    for (i=0;i<=3;i++)
    {
        if(Tank0_y[i]<=atan(z2)*Tank0_x[i]+k1[0]&&Tank0_y[i]>=atan(z2)*Tank0_x[i]+k1[1]&&Tank0_y[i]>=-tan(z2)*Tank0_x[i]+k1[3]&&Tank0_y[i]<=-tan(z2)*Tank0_x[i]+k1[2])
              //己方坦克撞到了敌方坦克
        {
            return 2;
            break;
        }
        else if(Tank1_y[i]<=atan(z1)*Tank1_x[i]+k0[0]&&Tank1_y[i]>=atan(z1)*Tank1_x[i]+k0[1]&&Tank1_y[i]>=-tan(z1)*Tank1_x[i]+k0[3]&&Tank1_y[i]<=-tan(z1)*Tank1_x[i]+k0[2])
             //敌方坦克撞到了己方坦克
        {
            return 2;
            break;
        }
    }
    return 1;
}
#endif // CHECK_MAP_H
