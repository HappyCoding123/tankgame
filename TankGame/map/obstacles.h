#ifndef OBSTACLES_H
#define OBSTACLES_H
#define area1 1
#define area2 2    //定义两个面积全局变量 便于以后修改

// 0 ：空白格 （墙壁类）{3：墙壁 4：铁壁} （debuff类）{5：沼泽 6：河流} （功能类）{7：桥(1234 上下左右) 8：穿越洞}（道具类）{10，11，12}

class Obstacles   //地形的基本要素
{
    int area;    //地形的面积
    int x,y;     //地形左上角的坐标
    int type;    //地形的类型
public:
    void set_location(int _x,int _y)    //设置地形的坐标
    {
        this->x=_x;
        this->y=_y;
    }
    void set_type(int _type)    //设置地形类型
    {
        this->type=_type;
        if(type==3||type==4||type==5||type==6||type==7||type==8)
            this->area=area1;
        else
            this->area=area2;
    }
    int get_x()                 //得到该地形的横坐标
    {
        return this->x;
    }
    int get_y()                 //得到该地形的纵坐标
    {
        return this->y;
    }
    int get_area()              //得到该地形的面积
    {
        return this->area;
    }
    int get_type()              //得到地形的类型
    {
        return this->type;
    }
    Obstacles();
};

/*class wall:public Obstacles          //墙壁类
{
    int blood;
public:
    void change_blood(int power)    //受到攻击改变墙壁的血量
    {
        int blood_middle;
        blood_middle=this->blood-power;
        if(blood_middle>0)
            this->blood=blood_middle;
        else
            this->blood=0;
    }
    int get_blood()                 //得到该地形的血量
    {
        return this->blood;
    }
};*/

class river:public Obstacles
{
    int style;
public:
    void set_style(int s)
    {
        this->style=s;
    }
    int get_style()
    {
        return this->style;
    }
};

#endif // OBSTACLES_H
