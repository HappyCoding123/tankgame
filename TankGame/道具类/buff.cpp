#include "buff.h"
#include<time.h>

int tankblood ;
Buff::Buff()
{
    x = 0;
    y = 0;
    buffexist = 0;
}

//道具状态
// buffexist = 0 表示道具未产生
// buffexist = 1 表示道具已经产生
// buffexist = 2 表示道具已经被吃
void Buff1::buff1create(QPainter *q){
   srand((unsigned int)(time(NULL)));
    if(rand() % 2 == 0){
    QPixmap img;
    img.load("../image/blood.png");           //贴buff图
       q->drawPixmap(this->x,this->y,20,20,img);        //这里用this->x,this->y代替敌方坦克的坐标
       Buff1::buffexist = 1;
    }
}

void Buff1::buff1act(){
    //如果坦克所在坐标与道具所在坐标一致 --道具状态改为2
    if (Buff1::buffexist == 2){
        tankblood += 50;            //坦克的血量增加50，这里用tankblood代替
        Buff1::buffexist = 0;     //重置
    }
}

void Buff2::buff2create(QPainter *q){
   srand((unsigned int)(time(NULL)));
    if(rand() % 2 == 0){
    QPixmap img;
    img.load("../image/bullettype2.png");           //贴buff图
       q->drawPixmap(this->x,this->y,20,20,img);        //这里用this->x,this->y代替敌方坦克的坐标
     Buff2::buffexist = 1;
    }
}

void Buff2::buff2act(){
    //如果坦克所在坐标与道具所在坐标一致 --道具状态改为2
    if (Buff2::buffexist == 2){
        //bullet2exist = 1;即激光的状态改为1
        Buff2::buffexist = 0;     //重置
    }
}

void Buff3::buff3create(QPainter *q){
    srand((unsigned int)(time(NULL)));
     if(rand() % 2 == 0){
     QPixmap img;
     img.load("../image/boom.png");           //贴buff图
        q->drawPixmap(this->x,this->y,20,20,img);        //这里用this->x,this->y代替敌方坦克的坐标
         Buff3::buffexist = 1;
     }
}

void Buff3::buff3act(){
    //如果坦克所在坐标与道具所在坐标一致 --道具状态改为2
    time_t start;
    start = time(NULL);
    while(time(NULL) - start < 10){             //存在十秒
    if (Buff3::buffexist == 2 ){
        tankblood -= 50;                //坦克的血量减少50，这里用tankblood代替
        break;
    }
    Buff3::buffexist = 0;     //重置
    }
}
